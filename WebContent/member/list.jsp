<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<%-- 나는 list.jsp 그러므로 리스트를 뿌려라
${memList} --%>
<table border="1">
		<tr>
			<th>번호</th>
			<th>이름</th>
			<th>ID</th>
			<!-- <th>PWD</th> -->
			<th>EMAIL</th>
			<th>TRANS</th>
			<th>생년월일</th>
			<th>우편번호</th>
			<th>주소</th>
			<th>상세주소</th>
			<th>가입일</th>
			<th>가입시간</th>
			<th>수정일</th>
			<th>수정시간</th>
			<th>이미지경로</th>
			<th>별명</th>
			<th>ETC</th>
		</tr>
		<c:forEach items="${memList}" var="member">
		<tr>
			<td>${member.miNum}</td>
			<td>${member.miName}</td>
			<td>${member.miId}</td>
			<%-- <td>${member.miPwd}</td> --%>
			<td>${member.miEmail}</td>
			<td>${member.miTrans}</td>
			<td>${member.miBirth}</td>
			<td>${member.miZipcode}</td>
			<td>${member.miAddr1}</td>
			<td>${member.miAddr2}</td>
			<td>${member.miCredat}</td>
			<td>${member.miCretim}</td>
			<td>${member.miModdat}</td>
			<td>${member.miModtim}</td>
			<td>
				<%-- <c:if test="${member.miMainImg ne ''}">
					<img src="/upload/${member.miMainImg}" width="100px"/>
					<!-- <img src="/upload/logo.png" width="100px"> -->
				</c:if>
				<c:if test="${member.miMainImg eq ''}">
					<img src="/upload/logo.png" width="100px"/>
				</c:if> --%>
				<img src="/upload/${member.miMainImg}" width="100px" onerror="this.src='/upload/logo.png'" alt="none">
			</td>
			<td>${member.miNick}</td>
			<td>${member.miEtc}</td>
		</tr>
		</c:forEach>
</table>
<img src="/upload/logo.png" width="100px">
</body>
</html>