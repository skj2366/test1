<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<script>
	<c:if test="${result=='true'}">
		alert('로그인 성공');
		location.href="http://localhost/mem/list.jsp";
	</c:if>
	<c:if test="${result=='false'}">
		alert('로그인 실패');
		location.href="http://localhost/member/login.jsp";
	</c:if>
</script>
</body>
</html>