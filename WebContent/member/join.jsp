<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>회원가입</title>
</head>
<body>
<form action="/mem/join" method="post" enctype="multipart/form-data">
	이름 : <input type="text" name="miName" id="miName" value="아무개"><br>
	아이디 : <input type="text" name="miId" id="miId" value="test"><br>
	비밀번호 : <input type="password" name="miPwd" id="miPwd" value="test"><br>
	이메일 : <input type="email" name="miEmail" id="miEmail" value="test@test"><br>
	성별 : <input type="number" name="miTrans" id="miTrans" value="1"><br>
	생년월일 : <input type="date" name="miBirth" id="miBirth" value="1992-02-29"><br>
	zipcode : <input type="number" name="miZipcode" id="miZipcode" value="12345"><br>
	Addr : <input type="text" name="miAddr1" id="miAddr1" value="test"><br>
	Addr2 : <input type="text" name="miAddr2" id="miAddr2" value="test"><br>
	이미지 : <input type="file" name="miMainImg" id="miMainImg"><br>
	닉네임 : <input type="text" name="miNick" id="miNick" value="test"><br>
	etc : <input type="text" name="miEtc"  id="miEtc" value="test"><br>
	<button type="submit">회원 가입</button>
</form>
</body>
</html>	