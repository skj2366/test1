<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html style="background-color: #323232;">
<head>
<meta charset="UTF-8">
<title>18번 채팅방</title>
<style>
	
	.purple-border{
		text-align: center;
	}
	
	.purple-border textarea {
    	border: 3px solid #ba68c8;
	}
	.purple-border .form-control {
	    border: 3px solid #FFF064;
	    box-shadow: 0 0 0 0.2rem rgba(186, 104, 200, .25);
	    background-color: #323232;
	    color: #00E1FF;
	    height: 300px;
	    width: 1200px;
	}
	
	.green-border-focus .form-control:focus {
	    border: 3px solid #8bc34a;
	    box-shadow: 0 0 0 0.2rem rgba(139, 195, 74, .25);
	}
	.indexDiv{
		background-color: #323232;
		padding: 30px;
		color: #00E1FF;
		font-size: 35px;
		text-align: center;
	}
	
	input[type=text] {
	  border: 3px solid #50B4FF;
	  border-radius: 4px;
	}
	
	button {
	  display: inline-block;
	  padding: 15px 25px;
	  font-size: 12px;
	  cursor: pointer;
	  text-align: center;
	  text-decoration: none;
	  outline: none;
	  color: #fff;
	  background-color: #4CAF50;
	  border: none;
	  border-radius: 15px;
	  box-shadow: 0 9px #999;
	}

	button:hover {background-color: #3e8e41}

	button:active {
	  background-color: #3e8e41;
	  box-shadow: 0 5px #666;
	  transform: translateY(4px);
	}
	
	#flexBtn{
		display: flex;
		
	}
</style>
</head>
<body>
<script>
	
	function clearRDiv(){
		document.querySelector('#rDiv').value ='';
	}
	
	function autoScroll(){
		document.querySelector('#rDiv').scrollTop = document.querySelector('#rDiv').scrollHeight;
	}

	function downloadInnerHtml(elementid,htmllinereplace,filename,mimeType,extension){
		var elHtml = $(elementid).html();
		  if (htmllinereplace) elHtml = elHtml.replace(/\<br\>/gi,'\n');
		  var link = document.createElement('a');
		  link.setAttribute('download', filename + extension);
		  link.setAttribute('href', 'data:' + mimeType  +  ';charset=utf-8,' + encodeURIComponent(elHtml));
		  link.click();
	}
	
	
	function enterkey() {
	    if (window.event.keyCode == 13) {
	    	sendMsg(document.querySelector('#btn'));
	    	//document.querySelector('#rDiv').scrollTop = document.querySelector('#rDiv').scrollHeight;
	    	autoScroll();
	    }
	}

	var socket;
	function sendMsg(btn){
		if(btn.innerHTML==='접속'){
			document.querySelector('#msg').disabled = false;
			document.querySelector('#clearBtn').disabled=false;
			var name = document.querySelector('#name').value
			socket = new WebSocket('ws://192.168.0.18/' + name);
			socket.onopen = function(evt){ 
				document.querySelector('#name').disabled = true;
				clearRDiv();
			}
			
			socket.onerror = function(e){
				alert('에러남!');
			}
			socket.onmessage = function(evt){
				var name = document.querySelector('#name').value
				var msg = JSON.parse(evt.data);
				var text = msg.name + ' : ' + msg.msg;
				if(msg.targetName){
					text = '[귓속말 : ' + text +']';
				}
				if(msg.open||msg.close){
					changeSelectBox(msg);
				} 
				
				if(msg.total=='1'){
					document.querySelector('#rDiv').value += '대화상대가 없습니다.' + '\n';	
				}else{
					document.querySelector('#rDiv').value += text + '\n';	
				}
				document.querySelector('#totalDiv').innerHTML = '총 : ' + msg.total + '명';
			}
			btn.innerHTML = '전송';
		}else if(btn.innerHTML==='전송'){
			var param = {};
			var msg = document.querySelector('#msg').value;
			param.msg = msg;
			if(document.querySelector('#names').value){
				param.targetName = document.querySelector('#names').value;
			}
			if(msg!=''){
				socket.send(JSON.stringify(param));	
				document.querySelector('#msg').value = '';
			}
			//document.querySelector('#rDiv').scrollTop = document.querySelector('#rDiv').scrollHeight;
			autoScroll();
		}
	}
	function changeSelectBox(msg){
		var name = document.querySelector('#name').value
		var names = msg.names;
		var options = '<option value="">=선택=</option>';
		for(var na of names){
			if(na!=name){
				options += '<option value="' + na +'">' + na + '</option>';
			}
		}
		console.log(options);
		console.log(names);
		document.querySelector('#names').innerHTML = options;
		document.querySelector('#names').style.display='';
	}
	
	function chatClose(){
		document.querySelector('#rDiv').style.display='none';
	}
	function chatOpen(){
		document.querySelector('#rDiv').style.display='block';
	}
	function chatFlex(){
		var btn = document.querySelector('#flexBtn');
		if(btn.innerHTML==='접기'){
			document.querySelector('#rDiv').style.display='none';
			btn.innerHTML = '펼치기';
		}else{
			document.querySelector('#rDiv').style.display='';
			btn.innerHTML = '접기';
		}
	}
</script>
<div class="indexDiv">
	<p>18번 채팅방</p>
</div>
<!-- <textarea id="rDiv" rows="20" cols="50"></textarea><br> -->
<div>
	<button onclick="chatFlex()" id="flexBtn">접기</button>
</div>
<div class="form-group purple-border">
  	<textarea class="form-control" id="rDiv" rows="20" cols="50" disabled="disabled">18번 채팅방에 오신것을 환영합니다.
닉네임을 입력하고 접속버튼을 눌러서 참여해주세요.</textarea><br>
  	<select id="names" style="display:none"></select>
 	<input type="text" id="name" placeholder="이름">
	<input type="text" id="msg" onkeypress="enterkey()" disabled="disabled">
	<button onclick="sendMsg(this)" id="btn">접속</button>
	<button onclick="clearRDiv()" disabled="disabled" id="clearBtn">대화내용지우기</button>
	<div id="totalDiv"></div>
</div>

</body>
</html>
