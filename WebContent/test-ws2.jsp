<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<style>
	.txtarea{
		background-color: pink;
		color: blue;
	}	
	.purple-border textarea {
    border: 3px solid #ba68c8;
	}
	.purple-border .form-control:focus {
	    border: 3px solid #ba68c8;
	    box-shadow: 0 0 0 0.2rem rgba(186, 104, 200, .25);
	}
	
	.green-border-focus .form-control:focus {
	    border: 3px solid #8bc34a;
	    box-shadow: 0 0 0 0.2rem rgba(139, 195, 74, .25);
	}
</style>
</head>
<body>
<script>

	function downloadInnerHtml(elementid,htmllinereplace,filename,mimeType,extension){
		var elHtml = $(elementid).html();
		  if (htmllinereplace) elHtml = elHtml.replace(/\<br\>/gi,'\n');
		  var link = document.createElement('a');
		  link.setAttribute('download', filename + extension);
		  link.setAttribute('href', 'data:' + mimeType  +  ';charset=utf-8,' + encodeURIComponent(elHtml));
		  link.click();
	}


	function enterkey() {
	    if (window.event.keyCode == 13) {
	    	sendMsg(document.querySelector('#btn'));
	    	document.querySelector('#rDiv').scrollTop = document.querySelector('#rDiv').scrollHeight;
	    }
	}
	
	var socket;
	function sendMsg(btn){
		if(btn.innerHTML==='접속'){
			var name = document.querySelector('#name').value
			socket = new WebSocket('ws://192.168.0.18/' + name);
			socket.onopen = function(e){
				document.querySelector('#rDiv').value += '====접속완료===\n';
			}
			
			socket.onerror = function(e){
				alert('에러남!');
			}
			socket.onmessage = function(evt){
				document.querySelector('#rDiv').value += evt.data + '\n';
			}
			btn.innerHTML = '전송';
		}else if(btn.innerHTML==='전송'){
			document.querySelector('#name').disabled;
			var msg = document.querySelector('#msg').value;
			if(msg!=''){
				socket.send(msg);	
			}
			document.querySelector('#msg').value = '';
		}
	}

</script>
<!-- <div id="rDiv" style="overflow: auto;"></div><br> -->
<div class="form-group purple-border">
  <textarea class="form-control" id="rDiv" rows="20" cols="50"></textarea>
</div>
<!-- <textarea id="rDiv" name="txtarea1" class="txtarea" rows="20" cols="50" ></textarea><br> -->
<input type="text" id="name" placeholder="이름">
<input type="text" id="msg" onkeypress="enterkey()">
<button id="btn" onclick="sendMsg(this)">접속</button>
</body>
</html>

