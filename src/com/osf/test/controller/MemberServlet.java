package com.osf.test.controller;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;

import com.osf.test.service.MemberService;
import com.osf.test.service.impl.MemberServiceImpl;
import com.osf.test.util.SHAEncoder;
import com.osf.test.vo.MemberVO;

@WebServlet("/mem/*")
public class MemberServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String PATH = "D:\\study\\workspace03\\test1\\WebContent\\upload";
//	private static final String PATH = "E:\\study\\gitlab\\test1\\WebContent\\upload";
	private MemberService ms = new MemberServiceImpl();

	public MemberServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String uri = request.getRequestURI();
		System.out.println(uri);
		uri = uri.replace(".jsp", "");
		uri = uri.replace("/mem/", "");
		System.out.println(uri);
		if ("list".equals(uri)) {
			request.setAttribute("memList", ms.selectMembers(null));
		}
		RequestDispatcher rd = request.getRequestDispatcher("/member/list.jsp");
		rd.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// 리퀘스트파싱
		String uri = request.getRequestURI();
		System.out.println(uri);
		uri = uri.replace(".jsp", "");
		uri = uri.replace("/mem/", "");
		System.out.println(uri);
		if ("join".equals(uri)) {
			ServletFileUpload sfu = new ServletFileUpload(new DiskFileItemFactory());
			try {
				List<FileItem> fiList = sfu.parseRequest(request);
				Map<String, String> param = new HashMap<>();
				for (FileItem fi : fiList) {
					if (fi.isFormField()) {
						String key = fi.getFieldName();
						String value = fi.getString("utf-8");
						param.put(key, value);
					} else {
							String key = fi.getFieldName();
//							String fileName = fi.getName();
							String fileName = "";
							if(fi.getSize()!=0) {
								fileName = Long.toString(System.nanoTime());
								String ext = FilenameUtils.getExtension(fi.getName());
								fileName += "." + ext;
							}
							param.put(key, fileName);
							File f = new File(PATH + "/" + fileName);
							try {
								fi.write(f);
							} catch (Exception e) {
								e.printStackTrace();
							}
					}
				}
				param.put("miPwd", SHAEncoder.encode(param.get("miPwd")));
				param.put("miBirth", param.get("miBirth").replace("-", ""));
				request.setAttribute("cnt", ms.insertMember(param));
				RequestDispatcher rd = request.getRequestDispatcher("/member/join_ok.jsp");
				rd.forward(request, response);
			} catch (FileUploadException e) {
				e.printStackTrace();
			}
		} else if ("login".equals(uri)) {
//			System.out.println("param : " + request.getParameter("miId"));
			MemberVO mvo = new MemberVO();
			mvo.setMiId(request.getParameter("miId"));
			mvo.setMiPwd(SHAEncoder.encode(request.getParameter("miPwd")));
			System.out.println(mvo.getMiId());
			System.out.println(mvo.getMiPwd());
			mvo = ms.selectMemberByIdAndPwd(mvo);
			System.out.println("mvo => " + mvo);

			if (mvo != null) {
				HttpSession hs = request.getSession();
				hs.setAttribute("member", mvo);
				request.setAttribute("result", "true");
			} else {
				request.setAttribute("result", "false");
			}
			RequestDispatcher rd = request.getRequestDispatcher("/member/login_ok.jsp");
			rd.forward(request, response);

		}

		// 리퀘스트파싱 종료

	}

}
